output "subnet_id" {
  value = aws_subnet.exort_subnet.id
}

output "vpc_id" {
  value = aws_vpc.exort_vpc.id
}