resource "aws_instance" "exort_33" {
  ami           = "ami-0705384c0b33c194c"
  instance_type = "t3.micro"
  subnet_id     = var.subnet_id
  vpc_security_group_ids = [var.security_group_id]
  associate_public_ip_address = true
  key_name                    = "ec2key"

  user_data = <<-EOF
                #!/bin/bash
                sudo apt update -y
                sudo apt install nginx1 -y
                sudo systemctl start nginx
                sudo systemctl enable nginx
              EOF

  tags = {
    Name = "exort_33_nginx_server"
  }
}

