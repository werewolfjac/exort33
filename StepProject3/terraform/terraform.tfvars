aws_region             = "eu-north-1"
name = "Madara"
ami_id = "ami-0705384c0b33c194c"
instance_type        = "t3.micro"
vpc_cidr           = "10.0.0.0/16"
vpc_azs = ["eu-north-1a", "eu-north-1b"]
vpc_private_subnets    = ["10.0.1.0/24", "10.0.2.0/24"]
vpc_public_subnets     = ["10.0.101.0/24", "10.0.102.0/24"]
enable_nat_gateway = true
single_nat_gateway = true
key_name    = "ec2key"
instance_count     = 3
tags = {
  Owner      = "madara"
  CreatedBy  = "madara"
  Purpose    = "step3"
}
ansible_user = "ubuntu"
ansible_port = 22
prometheus_port        = 9090
grafana_port           = 3000
node_exporter_port     = 9100
private_key            = "~/.ssh/ec2key.pem"
cadvisor_port          = 8080

