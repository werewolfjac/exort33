variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "prometheus_port" {
  type = number
  description = "Prometheus port"
}

variable "grafana_port" {
  type = number
  description = "Grafana port"
}

variable "node_exporter_port" {
  type = number
  description = "Node Exporter port"
}

variable "cadvisor_port" {
  type = number
  description = "cAdvisor port"
}

variable "tags" {
  description = "Tags to apply to resources"
  type        = map(string)
  default = {
    Owner      = "madara"
    CreatedBy  = "madara"
    Purpose    = "step3"
  }
}