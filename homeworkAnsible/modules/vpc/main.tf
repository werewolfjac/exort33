
resource "aws_vpc" "exort_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = var.name
  }
}

resource "aws_internet_gateway" "exort_igw" {
  vpc_id = aws_vpc.exort_vpc.id

    tags = {
    Name = "${var.name}-igw"
  }
}

resource "aws_subnet" "exort" {
  vpc_id                  = aws_vpc.exort_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "eu-north-1a"
  tags = {
    Name = "${var.name}-public-subnet"
  }
}



resource "aws_route_table" "public" {
  vpc_id = aws_vpc.exort_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.exort_igw.id
  }

  tags = {
    Name = "${var.name}-public-rt"
  }
}

resource "aws_route_table_association" "public" {

  subnet_id      = aws_subnet.exort.id
  route_table_id = aws_route_table.public.id
}



