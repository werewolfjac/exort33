pipeline {
    agent { label 'slave' }

    triggers {
        cron('H/5 * * * *') // Запускать конвейер каждые 5 минут
    }

    stages {
        stage('Checkout') {
            steps {
                echo 'Loading code from GitLab'
                git branch: 'main', credentialsId: 'jenkins-token', url: 'https://gitlab.com/werewolfjac/exort33.git'
            }
        }

        stage('Parallel') {
            parallel {
                stage('Test') {
                    agent {
                        docker { 
                            image 'python:3.9-slim'
                            args '--user root' // Для установки пакетов
                        }
                    }
                    steps {
                        echo 'Launch tests'
                        dir('lesson-20240215/python') {
                            sh """
                                pip3 install -r requirements.txt
                                python3 -m unittest discover -v
                                python3 -m coverage run -m unittest
                                python3 -m coverage report
                            """
                        }
                    }
                }
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    if (currentBuild.result == 'SUCCESS') {
                        sh 'docker build -t exort33/exort:latest .'
                    } else {
                        error 'Tests failed, Docker image will not be built'
                    }
                }
            }
        }

        stage('Push Docker Image to Docker Hub') {
            steps {
                script {
                    if (currentBuild.result == 'SUCCESS') {
                        withCredentials([usernamePassword(credentialsId: 'lap_docker_hub', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                            sh 'docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD'
                        }
                        sh 'docker push exort33/exort:latest'
                    } else {
                        error 'Tests failed, Docker image will not be pushed'
                    }
                }
            }
        }
    }
}
