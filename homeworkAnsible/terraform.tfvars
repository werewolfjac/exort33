ami                  = "ami-0705384c0b33c194c" 
instance_type        = "t3.micro"
key_name             = "ec2key"  
name                 = "exort"
open_ports           = [22, 443, 80, 9000, 90]
region               = "eu-north-1"
instance_count       = 2
nginx_port           = 8080