variable "name" {
  type = string
}

variable "instance_count" {
  type = number
}

variable "ami_id" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "vpc_security_group_ids" {
  type = list(string)
}

variable "subnet_ids" {
  type = list(string)
}

variable "ansible_user" {
  type = string
}

variable "ansible_port" {
  type = number
}

variable "private_key" {
  type = string
}

variable "tags" {
  description = "Tags to apply to resources"
  type        = map(string)
  default = {
    Owner      = "madara"
    CreatedBy  = "madara"
    Purpose    = "step3"
  }
}