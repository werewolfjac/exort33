terraform {
  backend "s3" {
    bucket         = "terraform-state-danit-devops-1"
    key            = "exort33/terraforma.tfstate"
    region         = "eu-central-1"
  }
}