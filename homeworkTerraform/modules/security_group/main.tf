resource "aws_security_group" "exort_allow_ports" {
  vpc_id      = var.vpc_id
  description = "acces to all open_ports"

  tags = {
    Name = "exort-open-ports"
  }
}

resource "aws_security_group_rule" "exort_ingress_rules" {
  count                    = length(var.open_ports)
  type                     = "ingress"
  from_port                = var.open_ports[count.index]
  to_port                  = var.open_ports[count.index]
  protocol                 = "tcp"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.exort_allow_ports.id
}

resource "aws_security_group_rule" "exort_egress_rules" {
  count                    = length(var.open_ports)
  type                     = "egress"
  from_port                = var.open_ports[count.index]
  to_port                  = var.open_ports[count.index]
  protocol                 = "tcp"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.exort_allow_ports.id
}

