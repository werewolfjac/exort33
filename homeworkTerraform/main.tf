module "exort_vpc" {
  source = "./modules/vpc"
}

module "exort_security_group" {
  source     = "./modules/security_group"
  vpc_id     = module.exort_vpc.vpc_id
  open_ports = var.open_ports
}

module "exort_ec2" {
  source            = "./modules/ec2"
  subnet_id         = module.exort_vpc.subnet_id
  security_group_id = module.exort_security_group.security_group_id
}

