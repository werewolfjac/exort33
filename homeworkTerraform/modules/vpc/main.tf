resource "aws_vpc" "exort_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "exort-vpc"
  }
}

resource "aws_subnet" "exort_subnet" {
  vpc_id            = aws_vpc.exort_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-north-1a"

  tags = {
    Name = "exort-subnet"
  }
}

resource "aws_internet_gateway" "exort_igw" {
  vpc_id = aws_vpc.exort_vpc.id

  tags = {
    Name = "exort-igw"
  }
}

resource "aws_route_table" "exort_route_table" {
  vpc_id = aws_vpc.exort_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.exort_igw.id
  }

  tags = {
    Name = "exort-route-table"
  }
}

resource "aws_route_table_association" "exort_association" {
  subnet_id      = aws_subnet.exort_subnet.id
  route_table_id = aws_route_table.exort_route_table.id
}