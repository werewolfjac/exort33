
output "instances" {
  value = aws_instance.exort[*].id
}

output "public_ips" {
  value = aws_instance.exort[*].public_ip
}